# Cara install 

1. Masuk ke dalam folder yang telah di clone dari github

2. Ketik "npm install" untuk mendownload semua package nya

3. Ketik "npm start" untuk menjalankan project atau dapat ketik "node index.js"

4. Untuk databases tidak perlu di buat lagi saya sudah menyediakan API untuk membuat databases dan collectionsnya.

5. Sekian terima kasih.

# List API

1. get : "/api/crt-db" tanpa petik itu untuk membuat database dan collectionsnya

2. post : "/api/insert/" tanpa petik dapat dijalankan pertama karena ini berfungsi untuk memasukan data ke dalama database

3. update : "/api/update" tanpa petik merupakan API untuk mengganti data dengan parameter slug.

4. delete : "/api/delete" tanpa petik merupakan API untuk menghapus data dalam databases dengan parameter slug.

5. get : "/api/get-all" tanpa petik merupakan API yang memunculkan seluruh data tanpa parameter namun di protected oleh JWT dan dapat mendapatkan aksesnya dengan mengunjungi API "/api/kunci-get-all" tanpa petik.

6. get : "/api/get" tanpa petik merupakan API yang memunculkan data utnuk pagination dengan memeberikan limit sampai dengan 10 data dan di lindungi oleh JWT utuk mengaksesnya dapat mengunjugi "/api/kunci-get" tanpa petik.

