var express = require("express");
var app = express();

var http = require("http").createServer(app);

var cors = require('cors')
var bp = require('body-parser')

var mongo = require('mongodb').MongoClient
var ObjectID = require('mongodb').ObjectID

var url = 'mongodb://127.0.0.1:27017/Baba';

var jwt = require("jsonwebtoken");

app.use(express.static(__dirname))
app.use(bp.json())
app.use(cors())

mongo.connect(url, {useNewUrlParser: true}, function(err, con) {    
    if(err) console.log('Koneksi database mengalami masalah :(\n');
    else{
        console.log('Koneksi database terhubung dengan baik :)\n');
    }
})

http.listen(4000, function(err){
    if(err){
        throw new Error(err)
    }
    console.log('Backend running in PORT=4000')
})

app.get("/api/crt-db", (req,res) => {
    mongo.connect(url, {useNewUrlParser: true}, function(err, con) {    
        if(err) console.log('Koneksi database mengalami masalah :(\n');
        else{
            var db = con.db("Baba");
            db.createCollection("Items", (err)=>{
                if(err) throw err;
                else{
                    res.send({
                        db: "Success created.",
                        collection: "Success created."
                    })                    
                    con.close();
                }
            })
        }
    })  
})

app.post("/api/insert" ,(req,res)=>{
    var data = {
        slug: new ObjectID(),
        name: req.body.name,
        desc: req.body.desc,
        price: req.body.price,
        image: [{
            img1: req.body.img1,
            img2: req.body.img2,
            img3: req.body.img3
        }
        ]
    }
    mongo.connect(url,{useNewUrlParser:true},(err,con)=>{
        if(err){
            res.sendStatus(404);
        }else{
            let itm = con.db("Baba").collection("items");
            itm.insertOne(data,(err)=>{
                if(err){res.sendStatus(404)}
                else{
                    res.send({status:"success"})
                }
            })
        }
    })
})

app.put("/api/update", (req,res)=>{
    var param = {
        slug: new ObjectID(req.body.id)
    }
    var data = {
        $set:{
            name: req.body.name,
            desc: req.body.desc,
            price: req.body.price,
            image: [{
                img1: req.body.img1,
                img2: req.body.img2,
                img3: req.body.img3
            }
            ] 
        }
    }
    mongo.connect(url,{useNewUrlParser:true},(err,con)=>{
        if(err){res.sendStatus(404)}
        else{
            let itm = con.db("Baba").collection("items");
            itm.updateOne(param,data, (err)=>{
                if(err){res.sendStatus(404)}
                else{
                    res.send({
                        status:"success"
                    })
                }
            })
        }
    })
})

app.delete("/api/delete",(req,res)=>{
    var param = {
        slug: req.body.id
    }
    mongo.connect(url,{useNewUrlParser:true},(err,con)=>{
        if(err){res.sendStatus(404)}
        else{
            let itm = con.db("Baba").collection("items");
            itm.deleteOne(param, (err)=>{
                if(err){res.sendStatus(404)}
                else{
                    res.send({
                        status:"success"
                    })
                }
            })
        }
    })
})

app.get("/api/kunci-get-all", (req,res)=>{
    mongo.connect(url,{useNewUrlParser:true},(err,con)=>{
        if(err){res.sendStatus(404)}
        else{
            let itm = con.db("Baba").collection("items");
            itm.find().toArray((err,row)=>{
                if(err){req.sendStatus(404)}
                else{
                    const data = row;
                    const token = jwt.sign({data},"kunci");
                    res.send({
                        status:"protected",
                        token:token
                    })
                }
            })
        }
    })
})

app.get("/api/get-all", ensureToken, (req,res)=>{
    jwt.verify(req.body.token,"kunci",(err,data)=>{
        if(err){
            res.sendStatus(404)
        }else{
            res.send({
                status:"success",
                data:data
            })
        }
    })
})

app.get("/api/kunci-get", (req,res)=>{
    var data = {
        slug: new ObjectID(req.body.id)
    }
    if(data === ""){
        res.sendStatus(404);
    }else{

        mongo.connect(url,{useNewUrlParser:true},(err,con)=>{
            if(err){res.sendStatus(404)}
            else{
                let itm = con.db("Baba").collection("items");
                itm.find(data).limit(10).toArray((err,row)=>{
                    if(err){req.sendStatus(404)}
                    else{
                        const data = row;
                        const token = jwt.sign({data},"kunci");
                        res.send({
                            status:"protected",
                            token:token
                        })
                    }
                })
            }
        })
    }    
})

app.get("/api/get", ensureToken, function(req,res){
    jwt.verify(req.token, "kunci", (err,data) => {
        if(err){res.sendStatus(404)}
        else{
            res.send({
                status:"success",
                data: data
            })
        }
    })
})

function ensureToken(req,res,next){
    const Header = req.headers["authorization"];
    if(typeof Header !== "undefined"){
        const split = Header.split(" ");
        const token = split[1];
        req.token = token
        next();
    }else{
        res.sendStatus(404);
    }
}